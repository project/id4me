<?php

namespace Drupal\id4me\Form;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\id4me\Id4meService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a ID4me form.
 */
final class LoginForm extends FormBase implements ContainerInjectionInterface {

  /**
   * The ID4me service object.
   *
   * @var \Drupal\id4me\Id4meService
   */
  protected $id4meService;

  /**
   * LoginForm constructor.
   *
   * @param \Drupal\id4me\Id4meService $id4me_service
   *   The ID4me service object.
   */
  public function __construct(
    Id4meService $id4me_service
  ) {
    $this->id4meService = $id4me_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new self(
      $container->get('id4me')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'id4me_login';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['identifier'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Enter your Identifier'),
      '#required' => TRUE,
    ];
    $form['actions'] = [
      '#type' => 'actions',
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Login with ID4me'),
    ];
    $form['#attached']['library'][] = 'id4me/id4me';
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    try {
      $this->id4meService
        ->setIdentifier($form_state->getValue('identifier'))
        ->discover();
    }
    catch (\Exception $e) {
      $form_state->setErrorByName('identifier', t('Please provide a valid ID4me identifier.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->id4meService
      ->setIdentifier($form_state->getValue('identifier'))
      ->discover()
      ->register();
    $response = $this->id4meService->authorize();
    $form_state->setResponse($response);
  }

}
